
import os
import re

import sys


skipdirs = ['\.git']
skipfiles = [__file__, '\.gitignore$', '\.txt$', '\.exe$', '\.md$']

tabd = 4

info = []


def isskipdir(d):
    for sd in skipdirs:
        if re.match(sd, d): return True

    return False

def isskipfile(f):
    for sf in skipfiles:
        if re.search(sf, f): return True

    return False

def dfs(path, tab):
    info.append([os.path.basename(path), 0, tab])
    i = len(info) - 1
    c = 0

    for d in os.listdir(path):
        newpath = path + os.path.sep + d

        if os.path.isdir(newpath):
            if not isskipdir(d):
                c += dfs(newpath, tab + tabd)
        elif not isskipfile(d):
            c += 1

    info[i][1] = c

    return c


dfs(os.getcwd(), 0)

pressanykey = False

if len(sys.argv) > 1:
    f = open(sys.argv[1], 'w')
else:
    f = sys.stdout
    pressanykey = True

for i in info:
    f.write('%s%s (%d)%s' % (' ' * i[2], i[0], i[1], os.linesep))

f.close()

if pressanykey: input(os.linesep + 'Press enter to exit')
